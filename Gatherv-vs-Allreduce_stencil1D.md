#### Short comparison of Allreduce and Gatherv for our 1D stencil

##### Allreduce (synchronize_full)

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|41 ms|24 ms|22 ms|29 ms|
|50000|565 ms|287 ms|155 ms|137 ms|
|100000|1120 ms|566 ms|295 ms|162 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|21 ms|14 ms|15 ms|18 ms|
|50000|293 ms|151 ms|85 ms|55 ms|
|100000|583 ms|297 ms|158 ms|96 ms|


##### Gatherv (mergeSubparts)

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|38 ms|22 ms|22 ms|24 ms|
|50000|568 ms|289 ms|156 ms|94 ms|
|100000|1126 ms|569 ms|297 ms|164 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|21 ms|14 ms|16 ms|21 ms|
|50000|293 ms|151 ms|86 ms|64 ms|
|100000|583 ms|297 ms|159 ms|98 ms|

So Gatherv seems to be a bit faster for a size of 10 000 in gcc whereas icc doesn't make much difference.
For sizes of 50 000 and 100 000 Allreduce seems to be equal or at least a bit faster in both gcc and icc.
Overall, there seems to be no big remarkable difference at first sight.

Tested on cluster: 10.12.18 ~18:00