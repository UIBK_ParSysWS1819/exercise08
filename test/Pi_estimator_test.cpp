#include <iostream>
#include <mpi.h>
#include <math.h>

#include "../source/PI_estimator/pi_estimator_mpi_reduce.h"


void test_pi(int pid, int num_processes) {
    int num_samples = 100000;
    double delta = 0.02;

    double pi = monte_carlo_pi_mpi(num_samples, num_processes, pid, 0);

    if (pid == 0) {
        if (std::abs(pi - M_PI) > delta) {
            throw std::runtime_error("Pi not in margin: \nCalculated Pi:" + std::to_string(pi) + "\nActual Pi: " + std::to_string(M_PI));
        }
    }
}

void test_hits() {
    int num_samples = 10000;
    int hits = monte_carlo_hits(num_samples);

    if (hits < 0 || hits > num_samples) {
        throw std::runtime_error("Number of hits not possible");
    }
}

int main(int argc, char** argv) {

    MPI::Status status;

    MPI::Init(argc, argv);  // Initialize MPI

    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    int num_tests = 10;
    for(int i=0; i<num_tests; i++) {
        test_hits();
        test_pi(pid, num_processes);
    }

    if (pid == 0) {
        std::cout << "Tests were successful" << std::endl;
    }

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}