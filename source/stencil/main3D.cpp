#include <iostream>
#include "mpi.h"
#include "stencil3D.h"
#include "../chrono_timer.h"


int main(int argc, char** argv) {

    MPI::Status status;

    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    // default values
    unsigned int size = 6;
    double north_boundary = 1.0;
    double east_boundary = 1.0;
    double south_boundary = -1.0;
    double west_boundary = 0.0;
    double upper_boundary = 0.0;
    double lower_boundary = 0.0;
    double epsilon_value = 10.0;
    
    if(argc == 9) {
        size = (unsigned) std::stoi(argv[1]);
        north_boundary = std::stod(argv[2]);
        east_boundary = std::stod(argv[3]);
        south_boundary = std::stod(argv[4]);
        west_boundary = std::stod(argv[5]);
        upper_boundary = std::stod(argv[6]);
        lower_boundary = std::stod(argv[7]);
        epsilon_value = std::stod(argv[8]);
    }
    else {
        /*std::cout << "No input! Default values will be used! "
                     "Usage: ./stencil3D "
                     "<size> "
                     "<north-boundary> "
                     "<east-boundary> "
                     "<south-boundary> "
                     "<west-boundary> "
                     "<upper-boundary> "
                     "<lower-boundary> "
                     "<epsilon-value>" << std::endl;*/
    }

    /*
    std::cout << "size: " << size <<
              "\nnorth boundary: " << north_boundary <<
              "\neast boundary: " << east_boundary <<
              "\nsouth boundary: " << south_boundary <<
              "\nwest boundary: " << west_boundary <<
              "\nupper boundary: " << upper_boundary <<
              "\nlower boundary: " << lower_boundary << 
              "\nepsilon value: " << epsilon_value << std::endl;
    */

    Stencil3D stencil3D(pid, pid_master, num_processes, size, north_boundary, east_boundary, south_boundary, west_boundary, upper_boundary, lower_boundary);

    ChronoTimer timer("MPI 3D Stencil");

    stencil3D.calculate_stencil(epsilon_value);

    long time = timer.getElapsedTime();

    stencil3D.synchronize_full();

    if (pid == pid_master) {
        //stencil3D.print_cube();
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;
    }

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}