#include <iostream>
#include "stencil1D.h"
#include "../chrono_timer.h"


int main(int argc, char** argv) {

    MPI::Status status;

    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.


    // default values
    unsigned int size = 512;
    double east_boundary = 0.5;
    double west_boundary = -0.5;
    double epsilon_value = 0.01;

    if(argc == 5) {
        size = (unsigned) std::stoi(argv[1]);
        east_boundary = std::stod(argv[2]);
        west_boundary = std::stod(argv[3]);
        epsilon_value = std::stod(argv[4]);
    }
    else {
//        std::cout << "No input! Default values will be used! "
//                     "Usage: ./stencil1D "
//                                "<size> "
//                                "<east-boundary> "
//                                "<west-boundary> "
//                                "<epsilon-value>" << std::endl;
    }


//    std::cout << "size: " << size <<
//        "\neast boundary: " << east_boundary <<
//        "\nwest boundary: " << west_boundary <<
//        "\nepsilon value: " << epsilon_value << std::endl;


    Stencil1D stencil1D(pid, pid_master, num_processes, size, east_boundary, west_boundary);

    ChronoTimer timer("MPI 1D Stencil");

    stencil1D.calculate_stencil(epsilon_value);

    long time = timer.getElapsedTime();

    stencil1D.synchronize_full();

    if (pid == pid_master) {
        //stencil1D.print_array();
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;
    }

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}