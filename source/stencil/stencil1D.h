#ifndef EXERCISE_07_STENCIL1D_H
#define EXERCISE_07_STENCIL1D_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <sstream>


class Stencil1D {
    int pid;
    int pid_master;
    int num_proc;
    unsigned int array_size;

    double east_boundary;
    double west_boundary;

    using Array = std::vector<double>;
    Array cells;

    double step_size;
    unsigned start_pos;
    unsigned end_pos;

public:
    Stencil1D(int pid, int pid_master, int num_proc, unsigned int size, double east, double west) :
            pid(pid), pid_master(pid_master), num_proc(num_proc), array_size(size + 2), east_boundary(east), west_boundary(west) {    // +2 for boundaries; operating area is then from 1 to N

        cells = Array(array_size);

        // setup boundaries
        set_boundaries();

        step_size = (double) (size) / (double) num_proc;
        start_pos = (unsigned) round(step_size * pid) + 1;
        end_pos = (unsigned) round(step_size * pid + step_size) + 1;

        // std::cout << "Created a Stencil 1D of size " << array_size << " (operating size: " << array_size-2 << ")\n" << std::endl;
    }

    ~Stencil1D() = default;

    void set_boundaries() {
        cells[0] = west_boundary;
        cells[array_size - 1] = east_boundary;
    }

    double jacobi_iteration_1d(const Array &in, Array &out, size_t size) {
        double sum_changes = 0;

        for(unsigned i = start_pos; i < end_pos; i++) {
            out[i] = (in[i  ] +         // current position
                      in[i-1] +         // position before current position
                      in[i+1]) / 3;     // position after current position
            sum_changes += std::abs(in[i] - out[i]);
        }

        return sum_changes;
    }


    void synchronization(Array &vect) {
        int tag = 0;

        // send to right, receive from left
        if (pid == 0) { // left
            MPI::COMM_WORLD.Send(&vect[end_pos - 1], 1, MPI::DOUBLE, pid + 1, tag);
        } else if (pid == num_proc - 1) { // right
            MPI::COMM_WORLD.Recv(&vect[start_pos - 1], 1, MPI::DOUBLE, pid - 1, tag);
        } else { // middle
            MPI::COMM_WORLD.Sendrecv(&vect[end_pos - 1], 1, MPI::DOUBLE, pid + 1, tag,
                                     &vect[start_pos - 1], 1, MPI::DOUBLE, pid - 1, tag);
        }

        // send to left, receive from right
        if (pid == 0) { // left
            MPI::COMM_WORLD.Recv(&vect[end_pos], 1, MPI::DOUBLE, pid + 1, tag);
        } else if (pid == num_proc - 1) { // right
            MPI::COMM_WORLD.Send(&vect[start_pos], 1, MPI::DOUBLE, pid - 1, tag);
        } else { // middle
            MPI::COMM_WORLD.Sendrecv(&vect[start_pos], 1, MPI::DOUBLE, pid - 1, tag,
                                     &vect[end_pos], 1, MPI::DOUBLE, pid + 1, tag);
        }

    }

    /*
     * Second possibility to merge sub parts to the whole result at the end.
     * The method currently in use can be seen in synchronize_full().
     */
    void mergeSubparts(Array &vect) {
        int tag = 0;

        // MPI::Gatherv structure using cells directly for total vector
        std::vector<double> myvec;
        int receive_counts[num_proc], receive_displacements[num_proc];

        // setup chunks corresponding to processes chunk position
        if (pid == 0){
            myvec = std::vector<double>(cells.begin() + start_pos -1, cells.begin() + end_pos); // incl. left boundary
        }else if (pid == num_proc - 1){
            myvec = std::vector<double>(cells.begin() + start_pos, cells.begin() + end_pos +1); // incl. right boundary
        }else{
            myvec = std::vector<double>(cells.begin() + start_pos, cells.begin() + end_pos);  // only my chunk
        }

//        std::cout << "PID " << pid << " : num_proc " << num_proc << std::endl;
//        std::cout << "PID " << pid << " : myvec " << printVector(myvec) << std::endl;
//        std::cout << "PID " << pid << " :" << "step_size " << step_size << std::endl;
//        std::cout << "PID " << pid << " :" << "start pos " << start_pos << std::endl;
//        std::cout << "PID " << pid << " :" << "end pos " << end_pos << std::endl;


        if (pid == pid_master) {
            double step_size_tmp = (double) (array_size -2) / (double) num_proc;
            unsigned start_pos_tmp = (unsigned) round(step_size * 0) + 1;
            unsigned end_pos_tmp = (unsigned) round(step_size * 0 + step_size) + 1;
            receive_displacements[0] = 0;
            receive_counts[0] = (end_pos_tmp - start_pos_tmp) +1;  // incl. left boundary
            // middle
            for(int i=1; i<num_proc; ++i) {
                start_pos_tmp = (unsigned) round(step_size * i) + 1;
                end_pos_tmp = (unsigned) round(step_size * i + step_size) + 1;
                if (i != num_proc -1){
                    receive_counts[i] = end_pos_tmp - start_pos_tmp;
                } else {
                    receive_counts[i] = end_pos_tmp - start_pos_tmp +1; // incl. right boundary
                }
                receive_displacements[i] = receive_displacements[i-1] + receive_counts[i-1];
            }
//            std::cout << "PID " << pid << " : receive_counts " << printArray(receive_counts,num_proc) << std::endl;
//            std::cout << "PID " << pid << " : receive_displacements "<< printArray(receive_displacements,num_proc) << std::endl;
//            std::cout << "PID " << pid << " : myvec "<< printVector(myvec) << std::endl;
        }

        MPI::COMM_WORLD.Gatherv(&myvec.front(), myvec.size(), MPI::DOUBLE, &cells.front(), receive_counts, receive_displacements, MPI::DOUBLE, pid_master);
//        if (pid == pid_master){
//            std::cout << "PID " << pid << " merged: " << printVector(cells);
//        }
    }

    bool iteration(double epsilon_value, const Array &in, Array &out) {
        bool break_out = false;

        double sum_changes = jacobi_iteration_1d(in, out, array_size);
        double total_changes = 0;

        MPI::COMM_WORLD.Reduce(&sum_changes, &total_changes, 1, MPI::DOUBLE, MPI::SUM, pid_master);

        if (pid == pid_master) {
            if (total_changes < epsilon_value) {
                break_out = true;
            }
        }

        // if master isn't the only participator in the computation, communicate with the others (slaves)
        if(num_proc > 1) {
            MPI::COMM_WORLD.Bcast(&break_out, 1, MPI::BOOL, pid_master);

            synchronization(out);
        }
        
        return break_out;
    }

    // maximum of the absolute values e.g. (-5, 3) -> -5
    static void max_abs(double* in, double* inout, const int* len, MPI::Datatype* dprt) {
        for(int i = 0; i < *len; i++) {
            inout[i] = (std::max(std::abs(in[i]), std::abs(inout[i])) == std::abs(in[i])) ? in[i] : inout[i];
        }
    }

    void synchronize_full() {
        MPI_Op max_abs_op;
        MPI_Op_create((MPI_User_function *) max_abs, 1, &max_abs_op);

        MPI::COMM_WORLD.Allreduce(MPI::IN_PLACE, &cells[0], array_size, MPI::DOUBLE, max_abs_op);

        MPI_Op_free(&max_abs_op);
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Array temp = cells;
        bool break_out;
        
        while (true) {
            break_out = iteration(epsilon_value, cells, temp);
            iterations++;
            
            if (break_out) {
                cells = temp;
                break;
            }

            break_out = iteration(epsilon_value, temp, cells);
            iterations++;
            
            if (break_out) {
                break;
            }
        }

        //mergeSubparts(cells);

        if (pid == pid_master) {
            std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
        }
    }

    const Array &getCells() const {
        return cells;
    }

    std::string printArray(const int *receive_counts, int size) const {
        std::ostringstream os;
        os << "[ ";
        for(auto i=0; i< size; i++){
            os << receive_counts[i] << " " ;
        }
        os << "]";
        return os.str();
    }

    template <typename T>
    std::string printVector(const std::vector<T>& v) const {
        std::ostringstream os;
        os << "< ";
        for(auto elem : v){
            os << elem << " " ;
        }
        os << ">";
        return os.str();
    }


    void print_array() {
        std::cout << printVector(cells) << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL1D_H
