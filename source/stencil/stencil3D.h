#ifndef EXERCISE_07_STENCIL3D_H
#define EXERCISE_07_STENCIL3D_H

#include <iostream>
#include <vector>
#include <cmath>
#include "mpi.h"


struct Cube : std::vector<double> {
    unsigned size;

    Cube() : size(0) {
        resize(0);
    }

    explicit Cube(unsigned n) : size(n) {
        resize(n * n * n);
    }

    double &operator()(int i, int j, int k) {
        return (*this)[i * size * size + j * size + k];
    }

    const double &operator()(int i, int j, int k) const {
        return (*this)[i * size * size + j * size + k];
    }
};

class Stencil3D {
    int pid;
    int pid_master;
    int num_proc;
    unsigned int cube_size;

    double north_boundary;
    double east_boundary;
    double south_boundary;
    double west_boundary;
    double upper_boundary;
    double lower_boundary;

    Cube cells = Cube();

    double step_size;
    unsigned start_pos;
    unsigned end_pos;

public:
    Stencil3D(int pid, int pid_master, int num_proc, unsigned int size, double north, double east, double south, double west,
              double upper, double lower) :
            pid(pid), pid_master(pid_master), num_proc(num_proc), cube_size(size + 2), north_boundary(north), east_boundary(east),
            south_boundary(south), west_boundary(west), upper_boundary(upper),
            lower_boundary(lower) {    // +2 for boundaries; operating area is then from 1 to N in both dimensions

        //cells = Cube(cube_size, std::vector<std::vector<double>>(cube_size, std::vector<double>(cube_size)));
        cells = Cube(cube_size);
        // setup boundaries
        set_boundaries();

        step_size = (double) (cube_size - 2) / (double) num_proc;
        start_pos = (unsigned) round(step_size * pid) + 1;
        end_pos = (unsigned) round(step_size * pid + step_size) + 1;

        // std::cout << "Created a Stencil 3D of size " << cube_size << "x" << cube_size << "x" << cube_size << "(operating size: " << cube_size-2 << "x" << cube_size-2 << "x" << cube_size-2 << ")\n" << std::endl;
    }

    ~Stencil3D() = default;

    void set_boundaries() {
        // set back pane (north) and front pane (south)
        for (unsigned j = 1; j < cube_size - 1; j++) {
            for (unsigned k = 1; k < cube_size - 1; k++) {
                cells(0, j, k) = north_boundary;            // back pane (north)
                cells(cube_size - 1, j, k) = south_boundary;  // front pane (south)
            }
        }

        // set left pane (west) and right pane (east)
        for (unsigned i = 1; i < cube_size - 1; i++) {
            for (unsigned k = 1; k < cube_size - 1; k++) {
                cells(i, 0, k) = west_boundary;             // left pane (west)
                cells(i, cube_size - 1, k) = east_boundary;   // right pane (east)
            }
        }

        // set lower and upper pane
        for (unsigned i = 1; i < cube_size - 1; i++) {
            for (unsigned j = 1; j < cube_size - 1; j++) {
                cells(i, j, 0) = lower_boundary;            // lower pane
                cells(i, j, cube_size - 1) = upper_boundary;  // upper pane
            }
        }
    }

    double jacobi_iteration_3d(const Cube &in, Cube &out, size_t size) {
        double sum_changes = 0;

        for (unsigned i = start_pos; i < end_pos; i++) {
            for (unsigned j = 1; j < size - 1; j++) {
                for (unsigned k = 1; k < size - 1; k++) {
                    out(i, j, k) = (in(i, j, k) +       // current position
                                    in(i - 1, j, k) +       // position northern to current position
                                    in(i + 1, j, k) +       // position southern to current position
                                    in(i, j - 1, k) +       // position western to current position
                                    in(i, j + 1, k) +       // position eastern to current position
                                    in(i, j, k - 1) +       // position in plane below current position
                                    in(i, j, k + 1)) / 7;   // position in plane above current position

                    //sum_changes += std::abs(out(i, j, k) - in(i, j, k));
                    sum_changes += std::abs(in(i, j, k) - out(i, j, k));
                }
            }
        }

        return sum_changes;
    }

    void synchronization(Cube &vect) {
        int tag = 0;

        // send to right, receive from left
        if (pid == 0) { // left
            MPI::COMM_WORLD.Send(&vect(end_pos - 1, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid + 1, tag);
        } else if (pid == num_proc - 1) { // right
            MPI::COMM_WORLD.Recv(&vect(start_pos - 1, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid - 1, tag);
        } else { // middle
            MPI::COMM_WORLD.Sendrecv(&vect(end_pos - 1, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid + 1, tag,
                                     &vect(start_pos - 1, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid - 1, tag);
        }

        // send to left, receive from right
        if (pid == 0) { // left
            MPI::COMM_WORLD.Recv(&vect(end_pos, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid + 1, tag);
        } else if (pid == num_proc - 1) { // right
            MPI::COMM_WORLD.Send(&vect(start_pos, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid - 1, tag);
        } else { // middle
            MPI::COMM_WORLD.Sendrecv(&vect(start_pos, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid - 1, tag,
                                     &vect(end_pos, 0, 0), cube_size * cube_size, MPI::DOUBLE, pid + 1, tag);
        }

    }

    bool iteration(double epsilon_value, const Cube &in, Cube &out) {
        bool break_out = false;

        double sum_changes = jacobi_iteration_3d(in, out, cube_size);

        double total_changes = 0;
        MPI::COMM_WORLD.Reduce(&sum_changes, &total_changes, 1, MPI::DOUBLE, MPI::SUM, pid_master);

        if (pid == pid_master) {
            if (total_changes < epsilon_value) {
                break_out = true;
            }
        }

        // if master isn't the only participator in the computation, communicate with the others (slaves)
        if(num_proc > 1) {
            MPI::COMM_WORLD.Bcast(&break_out, 1, MPI::BOOL, pid_master);

            synchronization(out);
        }
        
        return break_out;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Cube temp = cells;
        bool break_out;
        while (true) {
            break_out = iteration(epsilon_value, cells, temp);
            iterations++;
            
            if (break_out) {
                cells = temp;
                break;
            }
            
            break_out = iteration(epsilon_value, temp, cells);
            iterations++;

            if (break_out) {
                break;
            }
        }

        if (pid == pid_master) {
            std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
        }
    }

    // maximum of the absolute values e.g. (-5,3) -> -5
    static void max_abs(double *in, double *inout, int *len, MPI::Datatype *dprt) {
        for(int i=0; i<*len; i++) {
            inout[i] = (std::max(std::abs(in[i]), std::abs(inout[i])) == std::abs(in[i])) ? in[i] : inout[i];
        }
    }


    void synchronize_full() {
        MPI_Op max_abs_op;
        MPI_Op_create((MPI_User_function *) max_abs, 1, &max_abs_op);
        MPI::COMM_WORLD.Allreduce(MPI::IN_PLACE, &cells(0,0,0), cube_size*cube_size*cube_size, MPI::DOUBLE, max_abs_op);
        MPI_Op_free(&max_abs_op);
    }

    const Cube &getCells() const {
        return cells;
    }

    void print_cube() {
        for (unsigned i = 0; i < cube_size; i++) {
            for (unsigned j = 0; j < cube_size; j++) {
                for (unsigned k = 0; k < cube_size; k++) {
                    std::cout << cells(i, j, k) << " ";
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }
    }
};

#endif //EXERCISE_07_STENCIL3D_H
