#!/bin/bash


median () {
    # sort times
    times=($(printf '%ld\n' "${@}" | sort -n))
    size=${#times[@]}

    local res_val   # define local variable within function

    if (( $size % 2 == 0 )); then
        val1=${times[$size / 2 - 1]}
        val2=${times[$size / 2]}

        res_val=$(( ($val1 + $val2) / 2 ))
    else
        res_val=${times[ $size / 2 ]}
    fi

    echo "$res_val"     # return median value
}

printf "|Size|Seq|1S|2S|4S|8S|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

runs=7

for size in 10 100 1000 10000 100000 1000000 10000000 #100000000 200000000 500000000
do
    printf "|%s|" $size

    ######################################## Sequential ########################################

    times=()

    for (( run = 1; run <= $runs; run++ ))
    do
        out_seq=`../PI_estimator/pi_estimator_seq $size`

        times+=($out_seq)
    done

    median_val_seq=$(median ${times[@]})   # get return value of median

    printf "%s ms|" $median_val_seq

    ########################################## MPI ##############################################

    for nslots in 1 2 4 8
    do
        times=()

        for (( run = 1; run <= $runs; run++ ))
        do
            out_par=`mpirun -np $nslots ../PI_estimator/pi_estimator_mpi $size`
            #out_par=`mpirun -np $nslots ../PI_estimator/pi_estimator_mpi_reduce $size`

            times+=($out_par)
        done

        median_val_mpi=$(median ${times[@]})    # get return value of median

        printf "%s ms|" $median_val_mpi
    done

    printf "\n"
done
