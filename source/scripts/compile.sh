#!/bin/bash


echo 'Compiling gcc'

module load gcc/5.1.0
module load openmpi/2.1.1

g++ -std=c++11 -O3 ../PI_estimator/pi_estimator_seq.cpp -o ../PI_estimator/pi_estimator_seq
mpic++ -std=c++11 -O3 ../PI_estimator/pi_estimator_mpi.cpp -o ../PI_estimator/pi_estimator_mpi
mpic++ -std=c++11 -O3 ../PI_estimator/pi_estimator_mpi_reduce.cpp -o ../PI_estimator/pi_estimator_mpi_reduce

#g++ -std=c++11 -O3 -march=native ../stencil/main1D.cpp -o ../stencil/stencil1D_seq
mpic++ -std=c++11 -O3 -march=native ../stencil/main1D.cpp -o ../stencil/stencil1D_mpi

#g++ -std=c++11 -O3 -march=native ../stencil/main2D.cpp -o ../stencil/stencil2D_seq
mpic++ -std=c++11 -O3 -march=native ../stencil/main2D.cpp -o ../stencil/stencil2D_mpi

#g++ -std=c++11 -O3 -march=native ../stencil/main3D.cpp -o ../stencil/stencil3D_seq
mpic++ -std=c++11 -O3 -march=native ../stencil/main3D.cpp -o ../stencil/stencil3D_mpi

module unload openmpi/2.1.1
module unload gcc/5.1.0

#####################################################################################################################

echo 'Compiling icc'

module load intel/15.0
#module load openmpi/3.1.1  # intel/15.0 seems not to work with openmpi/3.1.1
module load openmpi/2.1.1

# PI estimator
icpc -std=c++11 -O3 ../PI_estimator/pi_estimator_seq.cpp -o ../PI_estimator/pi_estimator_seq_icc
mpic++ -std=c++11 -O3 ../PI_estimator/pi_estimator_mpi.cpp -o ../PI_estimator/pi_estimator_mpi_icc
mpic++ -std=c++11 -O3 ../PI_estimator/pi_estimator_mpi_reduce.cpp -o ../PI_estimator/pi_estimator_mpi_reduce_icc

# Stencil
#icpc -std=c++11 -O3 -march=native ../stencil/main1D.cpp -o ../stencil/stencil1D_seq_icc
mpic++ -std=c++11 -O3 -march=native ../stencil/main1D.cpp -o ../stencil/stencil1D_mpi_icc

#icpc -std=c++11 -O3 -march=native ../stencil/main2D.cpp -o ../stencil/stencil2D_seq_icc
mpic++ -std=c++11 -O3 -march=native ../stencil/main2D.cpp -o ../stencil/stencil2D_mpi_icc

#icpc -std=c++11 -O3 -march=native ../stencil/main3D.cpp -o ../stencil/stencil3D_seq_icc
mpic++ -std=c++11 -O3 -march=native ../stencil/main3D.cpp -o ../stencil/stencil3D_mpi_icc

#module unload openmpi/3.1.1    # intel/15.0 seems not to work with openmpi/3.1.1
module unload openmpi/2.1.1
module unload intel/15.0
