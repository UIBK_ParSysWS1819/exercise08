#!/bin/bash


median () {
    # sort times
    times=($(printf '%ld\n' "${@}" | sort -n))
    size=${#times[@]}

    local res_val   # define local variable within function

    if (( $size % 2 == 0 )); then
        val1=${times[$size / 2 - 1]}
        val2=${times[$size / 2]}

        res_val=$(( ($val1 + $val2) / 2 ))
    else
        res_val=${times[ $size / 2 ]}
    fi

    echo "$res_val"     # return median value
}

#printf "|Size|Seq|1S|2S|4S|8S|\n"
#printf "|---:|---:|---:|---:|---:|---:|\n"

printf "|Size|1S|2S|4S|8S|\n"
printf "|---:|---:|---:|---:|---:|\n"

runs=7

for size in 256 384 512
do
    printf "|%s|" $size

    ######################################## Sequential ########################################

    #times=()

    #for (( run = 1; run <= $runs; run++ ))
    #do
        #out_seq=`../stencil/stencil2D_seq $size 1.0 0.5 0.0 -0.5 10.0`

        #times+=($out_seq)
    #done

    #median_val_seq=$(median ${times[@]})   # get return value of median

    #printf "%s ms|" $median_val_seq

    ########################################## MPI ##############################################

    for nslots in 1 2 4 8
    do
        times=()

        for (( run = 1; run <= $runs; run++ ))
        do
            out_par=`mpirun -np $nslots ../stencil/stencil2D_mpi $size 1.0 0.5 0.0 -0.5 10.0`

            times+=($out_par)
        done

        median_val_mpi=$(median ${times[@]})    # get return value of median

        printf "%s ms|" $median_val_mpi
    done

    printf "\n"
done
