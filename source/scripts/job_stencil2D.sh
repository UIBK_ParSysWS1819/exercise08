#!/bin/bash

#$ -N stencil_2D

#$ -q std.q

#$ -cwd

#$ -o output_stencil_2D.txt

#$ -j yes

#$ -pe openmpi-fillup 8

module load gcc/5.1.0
module load openmpi/2.1.1
./bench_stencil2D.sh
module unload openmpi/2.1.1
module unload gcc/5.1.0

module load intel/15.0
module load openmpi/2.1.1
./bench_stencil2D_icc.sh
module unload openmpi/2.1.1
module unload intel/15.0