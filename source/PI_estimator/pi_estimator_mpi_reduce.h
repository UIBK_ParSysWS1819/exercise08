#ifndef PI_MPI_H
#define PI_MPI_H

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <mpi.h>
#include <random>


int monte_carlo_hits(int num_samples) {
    std::random_device random_device;
    std::ranlux48_base engine(random_device());
    std::uniform_real_distribution<double> uniform_dist(0, 1);

    int hits = 0;

    for(size_t i = 0; i < num_samples; i++) {
        double x = uniform_dist(engine);
        double y = uniform_dist(engine);
        double dist = x * x + y * y;

        if(dist <= 1.0) {
            hits++;
        }
    }

    return hits;
}

double monte_carlo_pi_mpi(int num_samples, int num_processes, int pid, int pid_master) {
    /*
     * This version uses MPI_Reduce instead of MPI_Send and MPI_Recv
     */
    int hits = monte_carlo_hits(num_samples / num_processes);
    int total_hits = 0;

    /*
     * Reduces values on all processes within a group
     * Parameters:
     * send buffer, Number of elements in send buffer, Data type of elements of send buffer, Reduce operation, Rank of root process
     * So here the root process with ID 0 has the result in the end.
     */
    MPI::COMM_WORLD.Reduce(&hits, &total_hits, 1, MPI::INT, MPI::SUM, pid_master);

    if(pid == pid_master) {   // The master process calculates the approximation of PI in the end
        double pi = ((double) total_hits * 4.0) / (double) num_samples;
        return pi;
    }
    
    return 0.0; // only the master returns pi
}

#endif