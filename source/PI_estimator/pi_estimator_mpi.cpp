#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <random>
#include <mpi.h>
#include "pi_estimator_mpi.h"


/*
 * A Monte Carlo estimator for pi. The input should be the number of samples to
 * use in the estimation, the output is the approximation of pi which was computed.
 */

// http://selkie-macalester.org/csinparallel/modules/MPIProgramming/build/html/calculatePi/Pi.html
// http://www.dartmouth.edu/~rc/classes/soft_dev/mpi.html
// https://www.olcf.ornl.gov/tutorials/monte-carlo-pi/
// https://github.com/vlexster/MPI-Monte-Carlo-Pi/blob/master/monte%20carlo%20pi.c


int main(int argc, char** argv) {
    int num_samples = 100000000; //default

    MPI::Status status;

    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    //std::cout << "Number of available processes: " << num_processes << std::endl;

    if(argc == 2) {
        num_samples = std::stoi(argv[1]);
    }

    std::cout.precision(10);

    //std::cout << "Number of samples: " << num_samples << std::endl;
    //std::cout << "Actual Pi: " << M_PI << std::endl;
    
    monte_carlo_pi_mpi(num_samples, num_processes, pid, pid_master);

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}