#ifndef EXERCISE08_PI_ESTIMATOR_MPI_H
#define EXERCISE08_PI_ESTIMATOR_MPI_H

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <random>
#include <mpi.h>
#include "../chrono_timer.h"


int monte_carlo_hits(int num_samples) {
    std::random_device random_device;
    std::ranlux48_base engine(random_device());
    std::uniform_real_distribution<double> uniform_dist(0, 1);

    int hits = 0;

    for(size_t i = 0; i < num_samples; i++) {
        double x = uniform_dist(engine);
        double y = uniform_dist(engine);
        double dist = x * x + y * y;

        if(dist <= 1.0) {
            hits++;
        }
    }

    return hits;
}

void mpi_master(int num_samples, int num_processes, int tag) {
    MPI::Status status;

    // The master also does some part of the computation (So we also have a result, if num_processes == 1)
    int hits_master = monte_carlo_hits(num_samples / num_processes);

    //std::cout << "Master got " << hits_master << " hits." << std::endl;

    // collect results from slaves
    int total_hits = hits_master;
    for(int process = 1; process < num_processes; process++) {
        int hits_current_slave = 0;

        MPI::COMM_WORLD.Recv(&hits_current_slave, 1, MPI::INT, MPI::ANY_SOURCE, tag, status);

        total_hits += hits_current_slave;

        //std::cout << "Got hits from slave: " << hits_current_slave << " - Total hits now: " << total_hits << std::endl;
    }

    double pi = ((double) total_hits * 4.0) / (double) num_samples;

    //std::cout << "Calculated Pi: " << pi << std::endl;
}

void mpi_slave(int num_samples, int num_processes, int pid, int pid_master, int tag) {
    int hits_current_slave = monte_carlo_hits(num_samples / num_processes);

    //std::cout << "Slave with PID " << pid << " got " << hits_current_slave << " hits. Sending it to master..." << std::endl;

    MPI::COMM_WORLD.Send(&hits_current_slave, 1, MPI::INT, pid_master, tag);
}

void monte_carlo_pi_mpi(int num_samples, int num_processes, int pid, int pid_master){
    int tag = 1;

    if(pid == pid_master) {   // The master process gathers the results from other processes...
        std::string str("Parallel Monte Carlo Pi master");
        ChronoTimer timer(str);

        mpi_master(num_samples, num_processes, tag);

        long time = timer.getElapsedTime();
        //std::cout << "Timer - " << str << ": " << time << " ms" << std::endl;
        std::cout << time << std::endl; // in ms
    }
    else {  // The other processes do the actual calculation and send their results to the master
        mpi_slave(num_samples, num_processes, pid, pid_master, tag);
    }
}

#endif //EXERCISE08_PI_ESTIMATOR_MPI_H
