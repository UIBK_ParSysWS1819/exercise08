#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include "../chrono_timer.h"


/*
 * A Monte Carlo estimator for pi. The input should be the number of samples to
 * use in the estimation, the output is the approximation of pi which was computed.
 */

unsigned int seed;

size_t monte_carlo_hits(size_t num_samples) {
    size_t hits = 0;
    for(size_t i = 0; i < num_samples; i++) {
        double x = (double)rand_r(&seed) / RAND_MAX;
        double y = (double)rand_r(&seed) / RAND_MAX;
        double dist = x*x + y*y;

        if(dist <= 1) {
            hits++;
        }
    }
    return hits;
}

double monte_carlo_pi_seq(size_t num_samples){
    size_t hits = monte_carlo_hits(num_samples);
    double pi = ((double)hits * 4.0) / (double)num_samples;

    return pi;
}


int main(int argc, char** argv) {
    int num_samples = 100000000; //default

    if(argc == 2) {
        num_samples = std::stoi(argv[1]);
    }

    std::cout.precision(10);

    //std::cout << "Number of samples: " << num_samples << std::endl;
    //std::cout << "Actual Pi: " << M_PI << std::endl;

    seed = time(NULL);


    std::string str("Sequential Monte Carlo Pi");
    ChronoTimer timer(str);

    double pi = monte_carlo_pi_seq(num_samples);

    long time = timer.getElapsedTime();

    //std::cout << "Timer - " << str << ": " << time << " ms" << std::endl;
    std::cout << time << std::endl; // in ms

    //std::cout << "Calculated Pi: " << pi << std::endl;

    return EXIT_SUCCESS;
}