## Monte Carlo Pi estimator
Since there ins't much communication in the monte carlo simulation, it is rather easy to parallelize.
Challenges were learning MPI, how it works and how it's used. We created 2 MPI versions of this program,
one using MPI send and receives and one using the MPI reduction function. Each process then takes care of
a portion of the total number of iterations. When they are done they then send their results to the master
process, which then calculates Pi.

## Stencil MPI
Parallelizing stencil brings new challenges, since after each iteration the processes have to communicate,
to share their results. We do this by using send & receive after each computing iteration. Each process has
its own part of the total dataset (array/matrix/cube), with its own start & end position. After the iteration
they send their boundaries to the neighbouring processes.

For calculating the number of changes in the whole system we use MPI reduce to gather all local changes. If the
break condition is then met, the master process broadcasts to everyone that they are done, so that each process
can break out of the main iteration loop.

Finally to gather the results we use a reduction on the whole dataset, so that each process gets the parts of the
other processes. For that we use a custom MPI reduction function which takes the value with the maximum absolute value.

A big challenge was that sending multidimensional vectors with MPI did not work out of the box. We therefore changed
our data structures to 1 dimensional vectors (similar to the 2nd exercise sheet).


## Results on cluster

Although it was not asked to do large scale benchmarking, we tried to measure our execution times in order
to show that the applications scale with increased number of slots on the cluster.

### Monte Carlo Pi estimator

#### Reduce version

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|Seq|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|1 ms|3 ms|2 ms|1 ms|0 ms|
|1000000|16 ms|38 ms|19 ms|9 ms|9 ms|
|10000000|160 ms|379 ms|190 ms|95 ms|55 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|Seq|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|1 ms|3 ms|2 ms|1 ms|1 ms|
|1000000|15 ms|32 ms|17 ms|8 ms|5 ms|
|10000000|151 ms|330 ms|166 ms|83 ms|46 ms|

#### Send-Receive version

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|Seq|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|1 ms|3 ms|2 ms|1 ms|0 ms|
|1000000|16 ms|37 ms|19 ms|9 ms|6 ms|
|10000000|160 ms|378 ms|189 ms|94 ms|49 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|Seq|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|1 ms|3 ms|1 ms|1 ms|1 ms|
|1000000|15 ms|28 ms|14 ms|7 ms|4 ms|
|10000000|151 ms|284 ms|142 ms|71 ms|40 ms|

As it can be seen, both variants perform pretty similar, especially for gcc. For icc you can
see a bit more difference.

### Stencil MPI

#### 1D

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|41 ms|23 ms|22 ms|25 ms|
|50000|565 ms|287 ms|155 ms|94 ms|
|100000|1120 ms|566 ms|295 ms|164 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|10000|21 ms|14 ms|15 ms|18 ms|
|50000|294 ms|151 ms|84 ms|59 ms|
|100000|583 ms|297 ms|158 ms|95 ms|

#### 2D

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|256|138 ms|72 ms|40 ms|25 ms|
|384|728 ms|374 ms|200 ms|113 ms|
|512|2344 ms|1212 ms|631 ms|349 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|256|79 ms|41 ms|24 ms|16 ms|
|384|409 ms|210 ms|114 ms|69 ms|
|512|1460 ms|753 ms|397 ms|226 ms|

#### 3D

openmpi/2.1.1 for compiler gcc-5.1.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|50|118 ms|62 ms|39 ms|31 ms|
|70|1044 ms|548 ms|305 ms|202 ms|
|90|5084 ms|2650 ms|1394 ms|876 ms|

openmpi/2.1.1 for compiler intel-15.0

|Size|1S|2S|4S|8S|
|---:|---:|---:|---:|---:|
|50|118 ms|62 ms|39 ms|30 ms|
|70|1040 ms|546 ms|304 ms|200 ms|
|90|5069 ms|2627 ms|1438 ms|877 ms|